rm(list=ls())

install.packages("dplyr")
install.packages("C50")
install.packages("gmodels")
install.packages("neuralnet")


DT <- function(fileName, ratio, seed) {
  
  #Storing filename into variable
  data <- read.csv(fileName, sep=";", stringsAsFactors = FALSE)  
  p <- data[0:10] #Predictors
  
  #Data Cleaning
  
  #Renames the columns (predictors & target variables)
  names(data) <- c("p1","p2","p3","p4","p5","p6","p7", "p8","p9", "p10","t1", "t2", "t3", "t4", "t5")
  names(p) <- c("p1","p2","p3","p4","p5","p6","p7", "p8","p9", "p10")
  
 
  
  #alcohol type
  # ex 1,0,0,0,0 = a  
  # 0,1,0,0,0 = b etc
  
  library(dplyr)
  
  #Create new column
  data <- mutate(data, type = "")
  
  #Subset is used to split the data according to which target (alcohol) is 1.
  #The type is set accordingly.
  a <- subset(data, t1 >= 1)
  a$type = "a"
  
  b <- subset(data, t2 >= 1)
  b$type = "b"
  
  c <- subset(data, t3 >= 1)
  c$type = "c"
  
  d <- subset(data, t4 >= 1)
  d$type = "d"
  
  e <- subset(data, t5 >= 1)
  e$type = "e"
  
  #Merge all subsets into one.
  data <- rbind(a,b,c,d,e)
  
  
  #dataset without target values only the type.
  # nd - New Dataset
  nd <- data[-c(11:15)]
  
  #Converting the target to a factor.
  nd$type <- as.factor(nd$type)
  
  
  #Data Splitting
  #, Shows all rows
  set.seed(seed)
  d.rows <- nrow(nd)
  
  #ratio represents the splitting amount ex (0.7 = 70 training : 30 testing.)
  d.sample <- sample(d.rows, d.rows * ratio) 
  d.train <- nd[d.sample, ] 
  d.test <- nd[-d.sample, ]
  
  #Model Creation
  library(C50)
  #C5.0(train without target, target, trails)
  d.model <- C5.0(d.train[-11], d.train$type, trails=10)
  
  #Prediction
  d.predict <- predict(d.model, d.test)
  
  #library to show confusion matrix
  library(gmodels)
  
  # Plot the confusion matrix
  # model evaluation (show confusion matrix etc.)
  CrossTable(d.test$type, d.predict, prop.chisq = FALSE, prop.c = FALSE, prop.r = FALSE, dnn=c("actual","predicted"))
}


normalize <- function(x) {
  return ( (x-min(x)) / (max(x)-min(x)))
} 

NN <- function(fileName, ratio, seed, NOfLayers1, NOfLayers2){
  #Storing filename
  data <- read.csv(fileName, sep=";", stringsAsFactors = FALSE)  
  
  #Data Cleaning

  #Names should be p for predicts & t for targets
  names(data) <- c("p1","p2","p3","p4","p5","p6","p7", "p8","p9", "p10",
                   "t1", "t2", "t3", "t4", "t5")
  
  #All data is normalsed between 0 & 1
  data_norm <- as.data.frame(lapply(data, normalize))
  
  
  #Data Splitting
  
  set.seed(seed)
  
  #The original data is not overidden, since eventually to use the model
  #the converted data has to be covnerted back.
  d.rows <- nrow(data_norm)
  d.sample <- sample(d.rows, d.rows * ratio)
  d.train <- data_norm[d.sample,]
  d.test <- data_norm[-d.sample,]
  
  library(neuralnet)
  
  #Model Creation
  d.model <- neuralnet(t1 + t2 + t3 + t4 + 
                         t5 ~ p1 + p2 + p3 + 
                         p4 + p5 + p6 + p7 +
                         p8 + p9 + p10, data = d.train,  hidden = c(NOfLayers1, NOfLayers2))
  
  # To plot the neural network
  #plot(d.model)
  
  
  #Evaluate results
  m.result <- compute(d.model, d.test[1:10])
  
  #Prediction Values
  predicted <- m.result$net.result
  #predicted
  
  originalValues <- max.col(d.test[, 11:15])
  predictedValues <- max.col(predicted)
  
  library(gmodels)
  # Plot the confusion matrix
  # model evaluation (show confusion matrix etc.)
  CrossTable(originalValues, predictedValues, prop.chisq = FALSE,
             prop.r = FALSE, prop.c=FALSE, dnn = c("actual","predicted"))
  
}


#Decision Tree
#File name, splitting ratio, seed

#Model 1
DT("QCM12.csv", 0.7,20)
DT("QCM12.csv", 0.7,50)
DT("QCM12.csv", 0.7,299)
DT("QCM12.csv", 0.7,521)
DT("QCM12.csv", 0.7,160)

#Model 2
DT("QCM12.csv", 0.6, 10)
DT("QCM12.csv", 0.6,20)
DT("QCM12.csv", 0.6,160)
DT("QCM12.csv", 0.6,50)
DT("QCM12.csv", 0.6, 336)


#Model 3
DT("QCM10.csv", 0.7,20)
DT("QCM10.csv", 0.7,50)
DT("QCM10.csv", 0.7,299)
DT("QCM10.csv", 0.7,160)
DT("QCM10.csv", 0.7,322)

#Model 4
DT("QCM10.csv", 0.6, 10)
DT("QCM10.csv", 0.6,20)
DT("QCM10.csv", 0.6,160)
DT("QCM10.csv", 0.6,50)
DT("QCM10.csv", 0.6,850)


#Model 5
DT("QCM3.csv", 0.7,20)
DT("QCM3.csv", 0.7,50)
DT("QCM3.csv", 0.7,299)
DT("QCM3.csv", 0.7,472)
DT("QCM3.csv", 0.7,876)

#Model 6
DT("QCM3.csv", 0.6,218)
DT("QCM3.csv", 0.6,220)
DT("QCM3.csv", 0.6,922)
DT("QCM3.csv", 0.6,117)
DT("QCM3.csv", 0.6,666)


#Neural Network
#Filename, splitting ratio, seed, number of layers 1, number of layers 2

#Model 1
NN("QCM12.csv", 0.7, 1002, 10, 5)
NN("QCM12.csv", 0.7, 50, 10, 5)
NN("QCM12.csv", 0.7, 11, 10, 5)
NN("QCM12.csv", 0.7, 100, 10, 5)
NN("QCM12.csv", 0.7, 22, 10, 5)

#Model 2
NN("QCM12.csv", 0.6, 1002, 20, 5)
NN("QCM12.csv", 0.6, 50, 20, 5)
NN("QCM12.csv", 0.6, 11, 20, 5)
NN("QCM12.csv", 0.6, 100, 20, 5)
NN("QCM12.csv", 0.6, 22, 20, 5)

#Model 3
NN("QCM10.csv", 0.7, 123, 30, 5)
NN("QCM10.csv", 0.7, 223, 30, 5)
NN("QCM10.csv", 0.7, 869, 30, 5)
NN("QCM10.csv", 0.7, 769, 30, 5)
NN("QCM10.csv", 0.7, 22, 30, 5)

#Model 4
NN("QCM10.csv", 0.6, 23, 20, 10)
NN("QCM10.csv", 0.6, 123, 20, 10)
NN("QCM10.csv", 0.6, 569, 20, 10)
NN("QCM10.csv", 0.6, 369, 20, 10)
NN("QCM10.csv", 0.6, 126, 20, 10)

#Model 5
NN("QCM3.csv", 0.7, 5, 30, 10)
NN("QCM3.csv", 0.7, 25, 30, 10)
NN("QCM3.csv", 0.7, 375, 30, 10)
NN("QCM3.csv", 0.7, 412, 30, 10)
NN("QCM3.csv", 0.7, 872, 30, 10)

#Model 6
NN("QCM3.csv", 0.6, 11, 10, 5)
NN("QCM3.csv", 0.6, 98, 10, 5)
NN("QCM3.csv", 0.6, 75, 10, 5)
NN("QCM3.csv", 0.6, 418, 10, 5)
NN("QCM3.csv", 0.6, 162, 10, 5)
